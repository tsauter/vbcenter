package main

type Machine struct {
	ID       string
	Name     string
	Host     string
	RunState int32
	RDPPort  int64
}

type Machines []Machine

func (m Machines) Len() int {
	return len(m)
}

func (m Machines) Swap(i, j int) {
	m[i], m[j] = m[j], m[i]
}

func (m Machines) Less(i, j int) bool {
	return m[i].Name < m[j].Name
}
