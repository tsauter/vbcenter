package main

import (
	"context"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"sort"
	"time"

	micro "github.com/micro/go-micro"
	"github.com/micro/go-micro/server"
	"github.com/spf13/viper"
	proto "gitlab.com/tsauter/vbcenteragent/proto"

	"github.com/gorilla/mux"
)

var (
	inventory InventoryService
	hostagent proto.VirtualBoxHostAgentService
)

func showMachines(w http.ResponseWriter, r *http.Request) {
	t, err := template.ParseFiles("machines.html")
	if err != nil {
		panic(err)
	}

	machines := inventory.GetMachines()
	sort.Sort(machines)

	if err := t.Execute(w, machines); err != nil {
		panic(err)
	}
}

func generateRDPFile(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]

	machine, err := inventory.GetMachineByID(id)
	if err != nil {
		fmt.Printf("error: %s\n", err)
		return
	}
	fmt.Printf("%#v\n", machine)

	if machine.RDPPort == 0 {
		fmt.Print("machine not configured for RDP")
		return
	}

	w.Header().Set("Content-Disposition", `attachment; filename="test.rdp"`)
	w.Write([]byte(fmt.Sprintf("full address:s:%s\n", machine.Host)))
	w.Write([]byte(fmt.Sprintf("server port:i:%d\n", machine.RDPPort)))
}

func update() {
	fmt.Printf("Updating inventory...")

	rsp, err := hostagent.GetMachines(context.TODO(), &proto.GetMachinesRequest{})
	if err != nil {
		fmt.Printf("Error: %s\n", err)
		return
	}

	// Print response
	machines := rsp.VM
	for _, m := range machines {
		newmachine := &Machine{
			ID:       m.ID,
			Name:     m.Name,
			Host:     m.Host,
			RunState: m.RunState,
			RDPPort:  m.RDPPort,
		}
		fmt.Printf("%#v\n", *newmachine)
		inventory.RegisterMachine(newmachine)
	}
}

func logWrapper(fn server.HandlerFunc) server.HandlerFunc {
	return func(ctx context.Context, req server.Request, rsp interface{}) error {
		log.Printf("[wrapper] server request: %v", req.Method())
		err := fn(ctx, req, rsp)
		return err
	}
}

func main() {
	viper.SetDefault("ListenAddress", ":9999")
	viper.SetConfigName("config")
	viper.AddConfigPath(".")
	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("Fatal error config file: %v", err))
	}

	inventory, err = NewInMemoryInventory()
	if err != nil {
		panic(err)
	}

	// Create a new service. Optionally include some options here.
	service := micro.NewService(
		micro.Name("virtualboxhostagent.client"),
		micro.WrapHandler(logWrapper),
	)
	service.Init()

	// Create new virtualboxhostagent client
	hostagent = proto.NewVirtualBoxHostAgentService("virtualboxhostagent", service.Client())

	go func() {
		update()
		timeTick := time.Tick(time.Second * 5)
		for {
			select {
			case <-timeTick:
				update()
			}
		}
	}()

	r := mux.NewRouter()

	r.HandleFunc("/rdp/{id}", generateRDPFile).Methods("GET")
	r.HandleFunc("/", showMachines).Methods("GET")

	listen := viper.GetString("ListenAddress")
	fmt.Printf("Listening on %s...\n", listen)
	if err := http.ListenAndServe(listen, r); err != nil {
		panic(err)
	}
}
