package main

type InventoryService interface {
	RegisterMachine(machine *Machine) error
	GetMachines() Machines
	GetMachineByID(id string) (*Machine, error)
}
