package main

import (
	"fmt"
	"sync"
)

type InMemoryInventory struct {
	m        sync.RWMutex
	Machines map[string]Machine
}

func NewInMemoryInventory() (*InMemoryInventory, error) {
	db := InMemoryInventory{}
	db.Machines = make(map[string]Machine)
	return &db, nil
}

func (imi *InMemoryInventory) RegisterMachine(machine *Machine) error {
	imi.m.Lock()
	defer imi.m.Unlock()

	key := machine.Host + "_" + machine.ID
	imi.Machines[key] = *machine

	return nil
}

func (imi *InMemoryInventory) GetMachines() Machines {
	imi.m.RLock()
	defer imi.m.RUnlock()

	var all []Machine
	for _, m := range imi.Machines {
		fmt.Printf("%v\n", m)
		all = append(all, m)
	}

	fmt.Printf("%v\n", all)
	return all
}

func (imi *InMemoryInventory) GetMachineByID(id string) (*Machine, error) {
	imi.m.RLock()
	defer imi.m.RUnlock()

	m, ok := imi.Machines[id]
	if !ok {
		return nil, fmt.Errorf("machine not found")
	}

	return &m, nil
}
